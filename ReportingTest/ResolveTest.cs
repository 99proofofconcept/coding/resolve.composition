﻿using FakeItEasy;
using Reporting.Contracts;
using Reporting.Kernel;
using Xunit;

namespace ReportingTest
{
    public class ResolveTest
    {
        [Fact]
        public void Should_resolve_the_type_header()
        {
            var container = A.Dummy<IResolver>();
            
            var instance = container.Resolve<IHeader>();

            Assert.IsAssignableFrom<IHeader>(instance);
        }

        [Fact]
        public void Should_resolve_the_type_footer()
        {
            var container = A.Dummy<IResolver>();
           
            var instance = container.Resolve<IFooter>();

            Assert.IsAssignableFrom<IFooter>(instance);
        }

        [Fact]
        public void Should_resolve_the_type_body()
        {
            var container = A.Dummy<IResolver>();
          
            var instance = container.Resolve<IBody>();

            Assert.IsAssignableFrom<IBody>(instance);
        }

        [Fact]
        public void Should_resolve_the_type_reporting()
        {
            var container = A.Dummy<IResolver>();
          
            var instance = container.Resolve<IReporting>();

            Assert.IsAssignableFrom<IReporting>(instance);
        }
    }
}
