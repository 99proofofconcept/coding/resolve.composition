﻿using System;
using FakeItEasy;
using Reporting;
using Reporting.Contracts;
using Xunit;

namespace ReportingTest
{
    internal class FakeHeader : IHeader { public string Content() => "Mocking header";}

    internal class FakeFooter : IFooter { public string Content() => "Mocking footer";}

    
    public class ReportingTest
    {
        
        [Fact]
        public void Should_throw_an_exception_when_body_of_report_is_null()
        {
            var report = A.Dummy<IReporting>();
            IBody body = null;

            A.CallTo( ()=> report.Generate(body)).Throws<ArgumentNullException>();
        }

       
        [Fact]
        public void Should_get_the_instance_of_the_resolver()
        {
            var reporting = new Report.Resolve<Report, FakeHeader, FakeFooter>().Reporting;

            Assert.IsAssignableFrom<IReporting>(reporting);
        }



        [Fact]
        public void Should_get_the_result_of_the_report_with_dependencies_injected()
        {
            var body = A.Fake<IBody>();
            var bodyContent = "This is a custom body";
            A.CallTo(() => body.Content()).Returns(bodyContent);

            string reportGenerated;
            using (var report = new Report.Resolve<Report, FakeHeader, FakeFooter>())
            {
                reportGenerated = report.Reporting.Generate(body);
            }
            
            Assert.NotNull(reportGenerated);
            Assert.Contains("Mocking header", reportGenerated);
            Assert.Contains(bodyContent, reportGenerated);
            Assert.Contains("Mocking footer", reportGenerated);
        }
        
    }

}
