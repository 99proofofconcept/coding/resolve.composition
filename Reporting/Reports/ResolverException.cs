﻿using System;
using System.Runtime.Serialization;

namespace Reporting.Kernel
{
#pragma warning disable S3925 // "ISerializable" should be implemented correctly
    public class ResolverException : Exception
#pragma warning restore S3925 // "ISerializable" should be implemented correctly
    {
        public ResolverException() { }

        public ResolverException(string message) : base(message) { }

        public ResolverException(string message, Exception inner) : base(message, inner) { }

        public ResolverException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
