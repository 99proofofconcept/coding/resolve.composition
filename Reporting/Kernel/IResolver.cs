﻿using System;

namespace Reporting.Kernel
{
    public interface IResolver : IDisposable
    {
        T Resolve<T>();
        void Register<TFrom, TTo>();
    }
}
