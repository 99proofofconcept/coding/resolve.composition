﻿namespace Reporting.Contracts
{
    public interface IHeader
    {
        string Content();
    }
}
